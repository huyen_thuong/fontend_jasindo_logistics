$('.slider_codepen .slider').slick({
    autoplay: true,
    autoplaySpeed: 1000,
    infinite: true,
    slidesToShow: 4,
    // centerMode: true,
    slidesToScroll: 1,
    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                // dots: true
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                centerMode: true,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                // centerMode: true,
                slidesToScroll: 1
            }
        }
    ]

});
// faq_menu
var home = {
    faqmenu: ['faq-general', 'faq-protecting', 'faq-search', 'faq-incoming', 'faq-requests']
};


$('.faq-menu-item').click(faqMenuShow);

function faqMenuShow(event) {

    faqpane = $(this).data('pane');
    //alert('The clicked faqpane is: '+faqpane);

    switch (faqpane) {
        case 'faq-general':

            // Do stuff to faq-general - easy stuff

            jQuery.each(home.faqmenu, function(index, value) {
                alert(this);
                return (this != "faq-general");
            });

            //^ What is NOT faq-general then remove this class: .parent().removeClass('faq-blue');

            break;

        case 'faq-protecting':

            $(this).addClass('faq-blue');

            break;
        case 'faq-search':

            $(this).addClass('faq-blue');

            break;
        case 'faq-incoming':

            $(this).addClass('faq-blue');

            break;
        case 'faq-requests':

            $(this).addClass('faq-blue');

            break;
    }

};